# Analyse des données de circulation de semences Graines del Païs

Ce dossier contient les différents scripts pour gérérer le document et
les figures relatifs à la circulation des semences entre les membres
de Graines del Païs.

Il part d'un fichier de données brutes issues de la base de données de
Graines del Païs, partiellement complété à la main avec l'aide de
Jean-Luc. Il parcours ensuite le jeu de données pour produire des
fichiers de ces données mises en forme pour procéder à l'analyse du
réseau d'échange de semences de Graines del Païs.

Structure des dossiers
===
La structure du dossier est la suivante :

 - data/
   - fichiers de données brutes issus de la base de données de GdP
   - fichiers de données mises en forme par les scripts
 - outputs/
   - fichier HTML produit par la compilation du fichier RMD
   - fichiers PDF contenant les différents graphes produits par
     l'analyse
 - src/
   - analysis.Rmd : fichier rassemblant les résultats d'analyse
   - dataClean.R : nettoyage des données brutes
   - extinction.R : calcul du taux d'extinction à partir du taux
     d'autoproduction
   - function_for_blockmodels.R : contient des fonctions d'analyse des
     graphes
   - matrixCreation.R : mise en forme des données brutes en matrice,
     plot du réseau
   - missingData.R : plot du taux de données manquantes au cours du
     temps dans la base de données

Compilation
====
Pour compiler le fichier HTML résumant les analyses, on peut utiliser
la commande	 `make`

Si l'on ne souhaite créer que le fichier _dataCleaned.csv_, on peut
utiliser la commande `make data/dataCleaned.csv`

Si l'on ne souhaite créer que le fichier _adjMatrix.csv_, on peut
utiliser la commande `make data/adjMatrix.csv`

Si l'on ne souhaite créer que le fichier _tauxExtinction.pdf_, on peut
utiliser la commande `make outputs/tauxExtinction.pdf`

Nettoyage du dossier
===

Pour supprimer l'ensemble des fichiers produits par les scripts et
repartir d'une version "propre", on peut utiliser la commande `make
clean`

Publication
===

Pour préparer à la "publication" du dossier et supprimer les fichiers
de données intermédiaires, on peut utiliser la commande `make publish`
