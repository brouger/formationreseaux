full: data/adjMatrix.csv data/dataCleaned.csv outputs/tauxExtinction.pdf README.md
	Rscript -e "rmarkdown::render('./src/analysis.Rmd', quiet=TRUE, clean=TRUE)"
	pandoc -o ReadMe.pdf README.md

entretien: data/dataCleaned.csv src/prepEntret.Rmd
	Rscript -e "rmarkdown::render('./src/prepEntret.Rmd', quiet=TRUE, clean=TRUE)"
	mv src/prepEntret.html outputs/prepEntret.html

data/dataCleaned.csv: src/dataClean.R data/gdp_tomates_lots_2019-09-25_v4.csv
	Rscript src/dataClean.R

outputs/tauxExtinction.pdf: src/extinction.R data/dataCleaned.csv
	Rscript src/extinction.R

data/adjMatrix.csv: src/matrixCreation.R data/dataCleaned.csv
	Rscript src/matrixCreation.R

clean:
	rm -f data/dataCleaned.csv\
		  data/adjMatrix.csv\
		  src/analysis.html\
		  outputs/ReseauGdPAnalyse.html\
		  outputs/reproGraph.pdf\
		  outputs/RepresReseau.pdf\
		  outputs/tauxExtinction.pdf\
		  outputs/TrackMissingData.pdf\
		  outputs/centralite.pdf
		  outputs/prepEntret.html

trackMissing: src/missingData.R
	Rscript src/missingData.R

publish:
	rm -f data/dataCleaned.csv\
		  data/adjMatrix.csv
	mv outputs/networks.pdf outputs/RepresReseau.pdf
	mv src/analysis.html outputs/ReseauGdPAnalyse.html
